<?php

/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */
//TODO ชื่อไม่ส่ื่อความหมาย
//TODO ตรง print ประกาศตัวแปลแล้วค่อยเรียกใช้
// \n PHP_EOL

function plusMinus($arr, $n)
{
    // Write your code here
    $positive = 0;
    $negative = 0;
    $zero = 0;

    for ($i = 0; $i < $n; $i++) {
        if ($arr[$i] > 0) {
            $positive++;
        } else if ($arr[$i] < 0) {
            $negative++;
        } else {
            $zero++;
        }
    }
    $positiveAvg = $positive / $n;
    $negativeAvg = $negative / $n;
    $zeroAvg = $zero / $n;



    print($positiveAvg . '\n' . $negativeAvg . '\n' . $zeroAvg);
}

$n = intval(trim(fgets(STDIN)));

$arr_temp = rtrim(fgets(STDIN));

$arr = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));

plusMinus($arr, $n);
