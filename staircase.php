<?php
//TODO ไม่ใช้ str_repeat
/*
 * Complete the 'staircase' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

    function staircase($n)
    {
        // Write your code here
        for ($i = 1; $i <= $n; $i++) {
            print(str_repeat(' ', $n-$i) .str_repeat('#', $i) . "\n");
        }
    }

    $n = intval(trim(fgets(STDIN)));

    staircase($n);
